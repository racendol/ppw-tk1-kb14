from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from .models import Info
from .forms import Info_Form
from .views import index


# Create your tests here.
	

class InfoModelsTest(TestCase):
	def test_if_index_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_url_using_func(self):
		found = resolve('/')
		self.assertEqual(found.func,index)

	def test_if_url_is_does_not_exist(self):
		response = Client().get('/NotExistPage/')
		self.assertEqual(response.status_code, 404)

	def test_index_use_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home.html')

	@classmethod
	def setUpTestData(cls):
		Info.objects.create(kota='Jakarta', suhu='tessuhu', 
        					udara='tesudara', kebakaran='teskebakaran',
        					banjir='tesbanjir', gempa='tesgempa',
        					peringatan='tesperingatan', langkah='teslangkah')

    
