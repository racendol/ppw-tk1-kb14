from django import forms
from django.forms import Form


KOTA_CHOICES = [
	("------","------"),
	('Jakarta', 'Jakarta'),
	('Surabaya','Surabaya'),
	('Palu','Palu')
	]

class Info_Form(forms.Form):

	pos_attrs = {
		'class':'form-control',
	}
	kota = forms.CharField(label='Pilih lokasi Anda', required=True, widget=forms.Select(choices=KOTA_CHOICES))
	suhu = forms.CharField(label='Suhu Udara', required=True)
	udara = forms.CharField(label='Kualitas Udara', required=True)
	kebakaran = forms.CharField(label='Kebakaran', required=False)
	banjir = forms.CharField(label='Banjir', required=False)
	gempa = forms.CharField(label='Gempa Bumi', required=False)
	peringatan = forms.CharField(required=False)
	langkah = forms.CharField(widget=forms.Textarea(attrs=pos_attrs), required=False, max_length=500)
	



