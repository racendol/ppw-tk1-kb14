from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from .forms import Info_Form
from .models import Info


# Create your views here.


def index(request):

	response = {'form': Info_Form}
	
	if request.method =='GET':
		form = Info_Form(request.GET)
		try:
			kota = request.GET.get('kota')
			response['info'] = Info.objects.get(kota=kota)
		except:
			response['info'] = Info.objects.get(kota = 'Jakarta')


		# if(form.is_valid()):
		# 	kota = form.cleaned_data['kota']
		# 	response['info'] = Info.objects.get(kota=kota)

		# else:
		# 	response['info'] = Info.objects.get(kota = 'Jakarta')

	else:
		response['info'] = Info.objects.get(kota = 'Jakarta')

	return render(request, 'home.html', response)


	
	# if (info == ''):

	# 	response['info'] = Info.objects.get(kota='Jakarta')

	# else:

	# 	response['info'] = Info.objects.get(kota=info)
	
	

# def pilih_lokasi(request):
# 	info = ''

# 	if (request.method =='POST'):
# 		form = Info_Form(request.POST)
		
# 		if(form.is_valid()):
# 			info = form.cleaned_data['kota']

# 	return redirect('index_kota', info)



