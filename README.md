# ppw-tk1-kb14
[![pipeline](https://gitlab.com/racendol/ppw-tk1-kb14/badges/master/pipeline.svg)](https://ppw2019-tk1-kb14.herokuapp.com/)
[![coverage report](https://gitlab.com/racendol/ppw-tk1-kb14/badges/master/coverage.svg)](https://gitlab.com/racendol/ppw-tk1-kb14/commits/master)

Tugas Kelompok 1 PPW 2019

Kelompok: KB14

Link herokuapp: https://ppw2019-tk1-kb14.herokuapp.com/


## Anggota

1. Fathur Rahman Prawira (1806186585)
2. Aan Nur Wahidi (1806141100)
3. Yasmin Adelia Puti Chaidir (1806141486)
4. Athiya Fatihah Akbar (1806186824)
5. Rocky Arkan Adnan Ahmad (1806186566)

## Nama Aplikasi
BeSafe

## Deskripsi Website

Indonesia merupakan negara yang indah, namun karena letak lokasi geografisnya, Indonesia menjadi negara 
yang rawan sekali menjadi langganan bencana alam. Bencana alam yang sering terjadi di Indonesia antara 
lain: banjir, polusi udara, gunung meletus, gempa bumi, dan tsunami. Dikarenakan industri 4.0 sedang 
berkembang pesat diikuti IoT (Internet of Things), maka kelompok kami berencana membuat suatu aplikasi 
mengenai kesiapsiagaan bencana. Aplikasi ini akan mengambil data yang berasal dari website BMKG. Website 
BMKG sendiri berisi informasi terkini mengenai kondisi alam di Indonesia. Cara kerja dari aplikasi ini 
adalah misalkan hari ini terjadi gempa bumi. Maka, dalam page Home akan memberikan peringatan tentang 
gempa dan sebuah mini poster bagaimana menyelamatkan diri dan apa saja hal yang perlu dilakukan dan dibawa. 
Manfaat yang akan didapat dari aplikasi kami adalah para pengguna yang khususnya adalah masyarakat Indonesi 
agar lebih paham mengenai kesiapsiagaan saat bencana alam terjadi. Karena masyarakat Indonesia masih minim 
pengetahuan akan hal ini, semoga aplikasi ini akan bermanfaat kedepannya.


## Daftar Fitur
Berikut fitur-fitur yang kami implementasikan:
1. Home, berisi informasi terkini lingkungan. Jika telah terjadi bencana, maka fitur ini otomatis akan 
   menampilkan apa saja yang perlu dilakukan saat bencana.
2. Mitigasi, berisi kumpulan poster mini dan video tentang kesiapsiagaan bencana.
3. Kontak Darurat, berisi kontak-kontak penting yang dapat dihubungi, seperti ambulance, pemadam kebakaran, 
   Basarnas, hingga organisasi independen untuk bantuan bencana alam.
4. Tentang Kami, berisi informasi mengenai sejarah website serta visi misi kami
5. Feedback, berisi tentang tanggapan, saran, kritik, dan pesan dari pembaca website untuk kami.


