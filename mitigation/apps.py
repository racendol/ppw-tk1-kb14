from django.apps import AppConfig


class MitigationConfig(AppConfig):
    name = 'mitigation'
