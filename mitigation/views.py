from django.shortcuts import render
from django.http import HttpResponse
from .models import Bencana
from .forms import BencanaForm

# Create your views here.

def index(request):
    response = {"bencana_form": BencanaForm}
    bencanas = Bencana.objects.all().values()
    response['bencanas'] = bencanas
    return render(request, 'mitigation.html', response)
    return HttpResponse("Mitigasi")

    if request.method == 'GET':
        form = BencanaForm(request.GET)
        try:
            bencana = request.GET.get('bencana')
            response['pilih bencana'] = Bencana.objects.get(bencana=bencana)
        except:
            response['pilih bencana'] = Bencana.objects.get(bencana='Emergency Kit')
    else:
        response['pilih bencana'] = Bencana.objects.get(bencana='Emergency Kit')

    return render(request, 'mitigation.html', response)
