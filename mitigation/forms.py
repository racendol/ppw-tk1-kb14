from django import forms

BENCANA_CHOICES = [
    ('Polusi Udara','Polusi Udara'),
    ('Banjir','Banjir'),
    ('Gempa Bumi','Gempa Bumi'),
    ('Kebakaran','Kebakaran')
     ]

class BencanaForm(forms.Form):
    category = forms.ChoiceField(
                        widget=forms.Select,
                        choices=BENCANA_CHOICES)
    
    attrs = {
        'class': 'form-control',
    }
