from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from .apps import FeedbackConfig
from .models import Feedback
from .forms import FeedbackForm
from .views import index, create_feedback
# Create your tests here

#url, forms, models, views, template
#assertin
class UnitTest(TestCase):
    def test_if_app_name_is_correct(self):
        self.assertEqual(FeedbackConfig.name, 'feedback')

    def test_if_model_is_registered(self):
        self.assertTrue(admin.site.is_registered(Feedback))

    def test_if_index_url_is_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, index)

    def test_if_create_feedback_url_is_exist(self):
        response = Client().get('/feedback/create_feedback/')
        self.assertEqual(response.status_code, 302)

    def test_create_feedback_url_using_func(self):
        found = resolve('/feedback/create_feedback/')
        self.assertEqual(found.func, create_feedback)

    def test_if_url_is_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_index_use_right_template(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback.html')

class FeedbackModelsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Feedback.objects.create(nama="tesnama",email="tesemail",isi="tesisi")
       
    def test_if_feedback_created(self):
        count = Feedback.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_if_feedback_exist(self):
        feedback = Feedback.objects.get(id=1)
        feedback_isi = feedback.isi
        self.assertEqual(feedback_isi, "tesisi")

    def test_if_new_is_in_database(self):
        new_feedback = Feedback.objects.create(nama="tesnama",email="tesemail",isi="tesisi")
        new_feedback.save()

        count = Feedback.objects.all().count()

        self.assertEqual(count, 2)
    
    
