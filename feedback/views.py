from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Feedback
from .forms import FeedbackForm
# Create your views here.
def index(request):
    response = {"feedback_form": FeedbackForm}
    feedbacks = Feedback.objects.all().values()
    response['feedbacks'] = feedbacks
    return render(request, 'feedback.html', response)

def create_feedback(request):
    if(request.method == 'POST'):
        form = FeedbackForm(request.POST)
        if (form.is_valid()):
            nama  = form.cleaned_data['nama']
            email = form.cleaned_data['email']
            isi = form.cleaned_data['isi']

            feedback = Feedback(nama = nama, email = email, isi = isi)
            feedback.save()

    return HttpResponseRedirect('/feedback/')
