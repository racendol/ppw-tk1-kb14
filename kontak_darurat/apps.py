from django.apps import AppConfig


class KontakDaruratConfig(AppConfig):
    name = 'kontak_darurat'
