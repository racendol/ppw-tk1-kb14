from django.db import models
from django import forms
from .models import Form
from django.forms import ModelForm

class Kontak_Form(ModelForm):
    class Meta:
        model = Form
        fields = "__all__"
