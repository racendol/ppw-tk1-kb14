from django.test import TestCase, Client
from django.contrib import admin 
from .models import Form
from .apps import KontakDaruratConfig
from .forms import Kontak_Form
from django.urls import resolve
from .views import index, add_forms

class PostModelTest(TestCase):
    def test_index_url_using_func(self):
        found = resolve('/kontak_darurat/')
        self.assertEqual(found.func, index)

    def test_add_forms_using_func(self):
        found = resolve('/kontak_darurat/add_forms')
        self.assertEqual(found.func, add_forms)

    def test_using_template_kontak_darurat(self):
        response = Client().get('/kontak_darurat/')
        self.assertTemplateUsed(response, 'kontak_darurat.html')

    def tes_using_template_forms(self):
        response = Client().get('/kontak_darurat/add_forms/')
        self.assertTemplateUsed(response, 'forms.html')

    def test_if_create_form_url_is_exist(self):
        response = Client().get('/kontak_darurat/add_forms')
        self.assertEqual(response.status_code, 200)

    def test_if_url_is_exist(self):
        response = Client().get('/kontak_darurat/')
        self.assertEquals(response.status_code, 200)

    def test_model_form_is_not_valid(self):
        form = Kontak_Form(data = {})
        self.assertFalse(form.is_valid())

    def test_if_url_is_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_if_keluhan_is_valid(self):
        data = {'nama': 'Aan', 'umur':'19', 'email':"aan@gmail.com",
                'kontak':"0890000000", 'alamat':'Jl.Mana', 'kota':'jkt',
                'rt':'10','rw':'10','kode_pos':'909090','Deskripsi_Kedaruratan':'sakit'}
        new_keluhan = Kontak_Form(data= data)
        self.assertTrue(new_keluhan.is_valid())

    

class FormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Form.objects.create(nama="Aan", umur="18", 
                                    email="aan@gmail.com",
                                    kontak="0000000000", alamat="dimana",
                                    kota="mana", rt="99", rw="99", kode_pos="14045",
                                    Deskripsi_Kedaruratan="Keluhan")

    def test_if_komentar_created(self):
        count = Form.objects.all().count()
        self.assertEqual(count, 1)

