from django.shortcuts import render
from django.http import HttpResponse
from .models import Form
from .forms import Kontak_Form
from django.forms import ModelForm
from django.contrib import messages

# Create your views here.

def index(request):
    return render(request, 'kontak_darurat.html', {})

def add_forms(request):
    response = {"form" : Kontak_Form}
    if(request.method == 'POST'):
        form = Kontak_Form(request.POST)
        if (form.is_valid()):
            data = form.save(commit=False)
            data.save()
            messages.success(request, 'Form submission successful')
    else:
        form = Kontak_Form()
    return render(request, 'forms.html', response)
