from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from .apps import AboutConfig
from .models import Komentar
from .forms import KomentarForm
from .views import index, create_comment, delete_comment
# Create your tests here

#url, forms, models, views, template
class UnitTest(TestCase):
    def test_if_index_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, index)

    def test_if_create_comment_url_is_exist(self):
        response = Client().get('/about/create_comment/')
        self.assertEqual(response.status_code, 302)

    def test_create_comment_url_using_func(self):
        found = resolve('/about/create_comment/')
        self.assertEqual(found.func, create_comment)

    def test_if_delete_comment_url_is_exist(self):
        response = Client().get('/about/delete_comment/1')
        self.assertEqual(response.status_code, 302)

    def test_delete_comment_url_using_func(self):
        found = resolve('/about/delete_comment/1')
        self.assertEqual(found.func, delete_comment)

    def test_if_url_is_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    
    def test_form_is_valid(self):
        form_data = {'komentar':'Sebuah Komentar'}
        form = KomentarForm(data = form_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['komentar'], 'Sebuah Komentar')

    def test_form_is_not_valid(self):
        form = KomentarForm(data = {})
        self.assertFalse(form.is_valid())

    def test_index_use_right_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_index_does_not_have_komentar(self):
        response = Client().get('/about/')
        self.assertContains(response, 'Belum ada komentar')
    
    def test_index_have_komentar(self):
        komentar = Komentar.objects.create(komentar='tes')
        komentar.save()
        response = Client().get('/about/')
        self.assertContains(response, 'tes')
    
    def test_index_create_komentar_valid(self):
        response = Client().post('/about/create_comment/', {'komentar':'Sebuah Komentar'})
        self.assertRedirects(response, '/about/')
        
        response2 = Client().get('/about/')
        self.assertContains(response2, 'Sebuah Komentar')

    def test_index_create_komentar_not_valid(self):
        response = Client().post('/about/create_comment/', {})
        self.assertRedirects(response, '/about/')

        response2 = Client().get('/about/')
        self.assertContains(response2, 'Belum ada komentar')

    def test_index_delete_komentar_valid(self):
        Client().post('/about/create_comment/', {'komentar':'Sebuah Komentar'})
        response = Client().get('/about/delete_comment/1')
        self.assertRedirects(response, '/about/')
        
        response2 = Client().get('/about/')
        self.assertContains(response2, 'Belum ada komentar')

    def test_index_delete_komentar_not_valid(self):
        Client().post('/about/create_comment/', {'komentar':'Sebuah Komentar'})
        response = Client().get('/about/delete_comment/2')
        self.assertRedirects(response, '/about/')
        
        response2 = Client().get('/about/')
        self.assertNotContains(response2, 'Belum ada komentar')
        self.assertContains(response2, 'Sebuah Komentar')

    
class KomentarModelsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Komentar.objects.create(komentar="Sebuah Komentar")

    def test_if_komentar_created(self):
        count = Komentar.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_if_komentar_exist(self):
        komentar = Komentar.objects.get(id=1)
        komentar_isi = komentar.komentar
        self.assertEqual(komentar_isi, "Sebuah Komentar")

    def test_if_komentar_max_lenth_is_50(self):
        komentar = Komentar.objects.get(id=1)
        komentar_length = komentar._meta.get_field('komentar').max_length
        self.assertEqual(komentar_length, 50)

    def test_if_new_is_in_database(self):
        new_komentar = Komentar(komentar = 'tes')
        new_komentar.save()

        count = Komentar.objects.all().count()

        self.assertEqual(count, 2)
    
    
