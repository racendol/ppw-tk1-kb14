from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Komentar
from .forms import KomentarForm
# Create your views here.

def index(request):
    response = {"komentar_form": KomentarForm}
    komentars = Komentar.objects.all().values()
    response['komentars'] = komentars
    return render(request, 'about.html', response)

def create_comment(request):
    if(request.method == 'POST'):
        form = KomentarForm(request.POST)
        if (form.is_valid()):
            komentar_isi = form.cleaned_data['komentar']
            komentar = Komentar(komentar = komentar_isi)
            komentar.save()

    return HttpResponseRedirect('/about/')

def delete_comment(request, komentar_id):
    try:
        komentar = Komentar.objects.get(id = komentar_id)
        komentar.delete()
    except:
        pass

    return HttpResponseRedirect('/about/')